<?php

namespace spec\TGF\Components\Command\Examples;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TGF\Components\Command\Examples\ExampleEndlessCommand;
use TGF\Components\Command\Examples\ExampleService;
use TGF\Util\Logger\LoggerInterface;

class ExampleOneCommandSpec extends ObjectBehavior
{
    public function let(
        ExampleService $service,
        LoggerInterface $logger,
        \PDO $pdo,
        InputInterface $input
    ): void {
        $this->beConstructedWith(
            $service,
            'example:command',
            $logger,
            $pdo,
            null
        );
        $input->bind(Argument::any())->willReturn(true);
        $input->isInteractive(Argument::any())->willReturn(true);
        $input->hasArgument(Argument::any())->willReturn(true);
        $input->getArgument(Argument::any())->willReturn(true);
        $input->validate(Argument::any())->willReturn(true);

        $logger->info('WORKER_STARTED')->willReturn(null);
        $logger->info('WORKER_FINISHED')->willReturn(null);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(ExampleEndlessCommand::class);
    }

    public function it_should_call_example_service(
        ExampleService $service,
        InputInterface $input,
        OutputInterface $output,
        LoggerInterface $logger
    ): void {
        $service->exampleMethod()->shouldBeCalledOnce();

        $logger->info('WORKER_STARTED')->shouldBeCalled();
        $logger->info('WORKER_FINISHED')->shouldBeCalled();

        $this->run($input, $output)->shouldReturn(0);
    }
}
