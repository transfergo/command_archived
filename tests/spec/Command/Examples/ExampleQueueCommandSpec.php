<?php

namespace spec\TGF\Components\Command\Examples;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TGF\Components\Command\Examples\ExampleEndlessCommand;
use TGF\Components\Command\Examples\ExampleService;
use TGF\Components\Command\Exception\LostConnectionException;
use TGF\Components\Command\Exception\SignalReceivedException;
use TGF\Util\Logger\LoggerInterface;

class ExampleQueueCommandSpec extends ObjectBehavior
{
    public function let(
        ExampleService $service,
        LoggerInterface $logger,
        \PDO $pdo,
        InputInterface $input
    ): void {
        $this->beConstructedWith(
            $service,
            'example:command',
            $logger,
            $pdo,
            null
        );
        $input->bind(Argument::any())->willReturn(true);
        $input->isInteractive(Argument::any())->willReturn(true);
        $input->hasArgument(Argument::any())->willReturn(true);
        $input->getArgument(Argument::any())->willReturn(true);
        $input->validate(Argument::any())->willReturn(true);

        $logger->info('WORKER_STARTED')->willReturn(null);
        $logger->info('WORKER_FINISHED')->willReturn(null);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(ExampleEndlessCommand::class);
    }

    public function it_should_support_run_once(
        ExampleService $service,
        InputInterface $input,
        OutputInterface $output,
        LoggerInterface $logger
    ): void {
        $input->getOption('run-once')->shouldBeCalled()->willReturn(true);

        $service->exampleMethod()->shouldBeCalledOnce();

        $logger->error(Argument::any())->shouldNotBeCalled();

        $logger->warning(Argument::any())->shouldNotBeCalled();

        $this->run($input, $output)->shouldReturn(0);
    }

    /**
     * run once
     * call support
     */

    public function it_should_exit_with_error_when_connection_was_lost(
        ExampleService $service,
        InputInterface $input,
        OutputInterface $output,
        LoggerInterface $logger
    ): void {
        $input->getOption('run-once')->shouldBeCalled()->willReturn(false);

        $service->exampleMethod()->willThrow(LostConnectionException::class);

        $logger->warning('WORKER_LOST_CONNECTION', Argument::type(\Exception::class))->shouldBeCalled();

        $this->run($input, $output)->shouldReturn(1);
    }

    public function it_should_exit_with_error_when_exception_was_thrown(
        ExampleService $service,
        InputInterface $input,
        OutputInterface $output,
        LoggerInterface $logger
    ): void {
        $input->getOption('run-once')->shouldBeCalled()->willReturn(false);

        $service->exampleMethod()->willThrow(\Exception::class);

        $logger->error('UNEXPECTED_WORKER_ERROR', Argument::type(\Exception::class))->shouldBeCalled();

        $this->run($input, $output)->shouldReturn(1);
    }

    public function it_should_gracefully_exit_if_signal_was_received(
        ExampleService $service,
        InputInterface $input,
        OutputInterface $output,
        LoggerInterface $logger
    ): void {
        $input->getOption('run-once')->shouldBeCalled()->willReturn(false);

        $service->exampleMethod()->willThrow(SignalReceivedException::class);

        $logger->info('WORKER_RECEIVED_SIGNAL', Argument::type(\Exception::class))->shouldBeCalled();

        $this->run($input, $output)->shouldReturn(0);
    }
}
