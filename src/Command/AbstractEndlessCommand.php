<?php declare(strict_types=1);

namespace TGF\Components\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TGF\Components\Command\Exception\LostConnectionException;
use TGF\Components\Command\Exception\SignalReceivedException;

abstract class AbstractEndlessCommand extends AbstractCommand
{
    abstract protected function getTickIntervalSeconds(): int;

    abstract protected function tick(): void;

    protected function configure(): void
    {
        $this->setDescription('Run endless command');

        $this->addOption(
            'run-once',
            null,
            InputOption::VALUE_OPTIONAL,
            'Exit after one iteration'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $runOnce = $input->getOption('run-once');

        try {
            while (true) {
                $tickStarted = microtime(true);

                $this->handleWorkerHealth();

                $this->newRelicStartTransaction();
                $this->tick();
                $this->newRelicEndTransaction();

                if ($runOnce) {
                    return self::ZERO_EXIT_CODE;
                }

                $this->sleep($tickStarted);
            }
        } catch (SignalReceivedException $e) {
            $this->logger->info('WORKER_RECEIVED_SIGNAL', $e);

            return self::ZERO_EXIT_CODE;
        } catch (LostConnectionException $e) {
            $this->logger->warning('WORKER_LOST_CONNECTION', $e);

            return self::NON_ZERO_EXIT_CODE;
        } catch (\Throwable $e) {
            $this->logger->error('UNEXPECTED_WORKER_ERROR', $e);

            return self::NON_ZERO_EXIT_CODE;
        }
    }

    private function sleep(float $tickStarted): void
    {
        $currentTime = microtime(true);
        $remainingSeconds = $this->getTickIntervalSeconds() - ($currentTime - $tickStarted);
        $remainingMicroseconds = (int)round($remainingSeconds * 1000000);

        if ($remainingMicroseconds > 0) {
            usleep($remainingMicroseconds);
        }
    }
}
