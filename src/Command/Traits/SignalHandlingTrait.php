<?php declare(strict_types=1);

namespace TGF\Components\Command\Traits;

use TGF\Components\Command\Exception\SignalReceivedException;
use TGF\Util\Logger\LoggerInterface;

/**
 * @property-read LoggerInterface $logger
 */
trait SignalHandlingTrait
{
    /** @var int */
    private $termSignalReceived = null;

    private function initSignalHandler(): void
    {
        if (!$this->isPcntlSupported()) {
            $this->logger->warning('DOCKER_MISSING_PCNTL_EXTENSION');

            return;
        }

        $handleSignal = function ($signal) {
            $this->logger->info('SIGNAL_RECEIVED');
            $this->termSignalReceived = $signal;
        };

        pcntl_async_signals(true);
        pcntl_signal(SIGTERM, $handleSignal);
        pcntl_signal(SIGQUIT, $handleSignal);
        pcntl_signal(SIGINT, $handleSignal);
        pcntl_signal(SIGWINCH, $handleSignal);
    }

    /**
     * @return bool
     */
    private function isPcntlSupported(): bool
    {
        if (false === \extension_loaded('pcntl')) {
            return false;
        }

        return \function_exists('pcntl_async_signals');
    }

    private function killWorkerIfTermSignalReceived(): void
    {
        if ($this->termSignalReceived === null) {
            return;
        }

        throw new SignalReceivedException(
            sprintf(
                'Signal %s',
                var_export($this->termSignalReceived, true)
            )
        );
    }
}
