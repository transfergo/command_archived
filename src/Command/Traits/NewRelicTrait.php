<?php declare(strict_types=1);

namespace TGF\Components\Command\Traits;

use TGF\Util\Logger\LoggerInterface;

/**
 * @property-read LoggerInterface $logger
 */
trait NewRelicTrait
{
    protected function checkNewRelicSupport(): void
    {
        if (!\extension_loaded('newrelic')) {
            $this->logger->warning('DOCKER_MISSING_NEW_RELIC_EXTENSION');
        }
    }

    protected function newRelicStartTransaction(): void
    {
        if (\extension_loaded('newrelic')) {
            \newrelic_start_transaction(ini_get('newrelic.appname'));
        }
    }

    protected function newRelicEndTransaction(): void
    {
        if (\extension_loaded('newrelic')) {
            \newrelic_end_transaction();
        }
    }

    protected function newRelicNameTransaction(string $name): void
    {
        if (\extension_loaded('newrelic')) {
            \newrelic_name_transaction($name);
        }
    }

    protected function newRelicBackgroundJob(): void
    {
        if (\extension_loaded('newrelic')) {
            \newrelic_background_job(true);
        }
    }
}
