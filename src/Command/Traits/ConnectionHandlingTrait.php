<?php declare(strict_types=1);

namespace TGF\Components\Command\Traits;

use Doctrine\Common\Persistence\Mapping\MappingException;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use TGF\Components\Command\Exception\LostConnectionException;
use TGF\Util\Logger\LoggerInterface;

/**
 * @property-read LoggerInterface $logger
 * @property-read EntityManager $entityManager
 * @property-read \PDO $pdo
 */
trait ConnectionHandlingTrait
{
    private function killWorkerIfEntityManagerClosed(): void
    {
        if (!$this->entityManager) {
            return;
        }

        if ($this->entityManager->isOpen()) {
            return;
        }

        throw new LostConnectionException('Entity manager is closed');
    }

    private function killWorkerIfMysqlConnectionLost(): void
    {
        $connection = null;

        if ($this->entityManager !== null) {
            $connection = $this->entityManager->getConnection();
        }

        if ($this->pdo !== null) {
            $connection = $this->pdo;
        }

        if ($connection === null) {
            return;
        }

        try {
            $connection->query('SELECT 1')->execute();
        } catch (DBALException | \PDOException $e) {
            throw new LostConnectionException(
                'Connection lost',
                0,
                $e
            );
        }
    }

    private function clearEntityManager(): void
    {
        if ($this->entityManager === null) {
            return;
        }

        try {
            $this->entityManager->clear();
        } catch (MappingException $e) {
            throw new LostConnectionException(
                'Error while clearing entity manager',
                0,
                $e
            );
        }
    }
}
