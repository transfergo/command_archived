<?php declare(strict_types=1);

namespace TGF\Components\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TGF\Components\Command\Traits\ConnectionHandlingTrait;
use TGF\Components\Command\Traits\NewRelicTrait;
use TGF\Components\Command\Traits\SignalHandlingTrait;
use TGF\Util\Logger\LoggerInterface;

abstract class AbstractCommand extends Command
{
    use SignalHandlingTrait;
    use ConnectionHandlingTrait;
    use NewRelicTrait;
    protected const ZERO_EXIT_CODE = 0;
    protected const NON_ZERO_EXIT_CODE = 1;

    /** @var null|EntityManager */
    protected $entityManager;

    /** @var LoggerInterface */
    protected $logger;

    /** @var null|\PDO */
    protected $pdo;

    /** @var bool */
    private $workerInitialized = false;

    public function __construct(
        string $commandName,
        LoggerInterface $logger,
        ?\PDO $pdo,
        ?EntityManager $entityManager
    ) {
        parent::__construct($commandName);

        $this->logger = $logger;
        $this->pdo = $pdo;
        $this->entityManager = $entityManager;
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->checkNewRelicSupport();
        $this->newRelicNameTransaction($this->getName());
        $this->newRelicBackgroundJob();

        $this->initSignalHandler();

        $this->workerInitialized = true;

        $this->logger->info('WORKER_STARTED');
    }

    protected function handleWorkerHealth(): void
    {
        $this->clearEntityManager();

        $this->killWorkerIfTermSignalReceived();
        $this->killWorkerIfMysqlConnectionLost();
        $this->killWorkerIfEntityManagerClosed();
    }

    public function __destruct()
    {
        if ($this->workerInitialized) {
            $this->logger->info('WORKER_FINISHED');
        }
    }
}
