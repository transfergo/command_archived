<?php declare(strict_types=1);

namespace TGF\Components\Command\Exception;

class LostConnectionException extends \RuntimeException
{
}
