<?php

declare(strict_types=1);

namespace TGF\Components\Command\Exception;

use RuntimeException;
use Throwable;

class InvalidJobException extends RuntimeException
{
    /** @var array */
    private $jobBody;

    /** @var string */
    private $name;

    /**
     * @param string $name
     * @param array $jobBody
     * @param Throwable|null $previous
     */
    public function __construct(?string $name, array $jobBody, Throwable $previous)
    {
        parent::__construct('Invalid job', 0, $previous);
        $this->name = $name;
        $this->jobBody = $jobBody;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getJobBody(): array
    {
        return $this->jobBody;
    }
}
