<?php declare(strict_types=1);

namespace TGF\Components\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TGF\Components\Command\Exception\InvalidJobException;
use TGF\Components\Command\Exception\LostConnectionException;
use TGF\Components\Command\Exception\SignalReceivedException;
use TGF\Util\IncomingQueue\Job;
use TGF\Util\IncomingQueue\Queue;
use TGF\Util\Logger\LoggerInterface;

abstract class AbstractQueueCommand extends AbstractCommand
{
    /**
     * AWS gives 10 seconds before sending SIGKILL
     * In order to avoid hard kills, it should be less
     */
    protected const POLLING_TIMEOUT = 5;

    /** @var Queue */
    private $queue;

    public function __construct(
        string $commandName,
        Queue $queue,
        LoggerInterface $logger,
        ?\PDO $pdo,
        ?EntityManager $entityManager
    ) {
        parent::__construct(
            $commandName,
            $logger,
            $pdo,
            $entityManager
        );

        $this->queue = $queue;
    }

    /**
     * @param Job $job
     */
    abstract protected function handleJob(Job $job): void;

    protected function configure(): void
    {
        $this->setDescription('Run queue listener');

        $this->addOption(
            'run-once',
            null,
            InputOption::VALUE_OPTIONAL,
            'Exit after one iteration'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $runOnce = $input->getOption('run-once');

        try {
            while (true) {
                $this->pollJobsFromQueue();

                if ($runOnce) {
                    return self::ZERO_EXIT_CODE;
                }
            }
        } catch (InvalidJobException $e) {
            $this->logger->warning(
                'WORKER_RECEIVED_INVALID_JOB',
                [
                    'name' => $e->getName(),
                    'jobBody' => $e->getJobBody(),
                ],
                $e
            );

            return self::NON_ZERO_EXIT_CODE;
        } catch (SignalReceivedException $e) {
            $this->logger->info('WORKER_RECEIVED_SIGNAL', $e);

            return self::ZERO_EXIT_CODE;
        } catch (LostConnectionException $e) {
            $this->logger->warning('WORKER_LOST_CONNECTION', $e);

            return self::NON_ZERO_EXIT_CODE;
        } catch (\Throwable $e) {
            $this->logger->error('UNEXPECTED_WORKER_ERROR', $e);

            return self::NON_ZERO_EXIT_CODE;
        }
    }

    private function pollJobsFromQueue(): void
    {
        $pollingStarted = microtime(true);

        $jobs = $this->queue->getJobsWithWaiting(self::POLLING_TIMEOUT);

        if (\count($jobs) === 0) {
            $this->handleWorkerHealth();

            $this->throttlePolling($pollingStarted);
        }

        foreach ($jobs as $job) {
            $this->handleWorkerHealth();

            try {
                $this->newRelicStartTransaction();

                $this->handleJob($job);
                $this->queue->delete($job);

                $this->newRelicEndTransaction();
            } catch (\Throwable $e) {
                throw new InvalidJobException(
                    $job->getBody()['eventName'] ?? null,
                    $job->getBody(),
                    $e
                );
            }
        }
    }

    private function throttlePolling(float $pollingStarted): void
    {
        $pollingEnded = microtime(true);
        $remainingTimeout = $pollingEnded - $pollingStarted - self::POLLING_TIMEOUT;

        if ($remainingTimeout > 0) {
            sleep((int) $remainingTimeout);
        }
    }
}
