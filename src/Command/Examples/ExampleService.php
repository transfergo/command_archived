<?php declare(strict_types=1);

namespace TGF\Components\Command\Examples;

class ExampleService
{
    public function exampleMethod($arg = null): void {
        // do nothing.
    }
}
