<?php declare(strict_types=1);

namespace TGF\Components\Command\Examples;

use Doctrine\ORM\EntityManager;
use TGF\Components\Command\AbstractEndlessCommand;
use TGF\Util\Logger\LoggerInterface;

class ExampleEndlessCommand extends AbstractEndlessCommand
{
    /** @var ExampleService */
    private $service;

    public function __construct(
        ExampleService $service,
        string $commandName,
        LoggerInterface $logger,
        ?\PDO $pdo,
        ?EntityManager $entityManager
    ) {
        parent::__construct($commandName, $logger, $pdo, $entityManager);
        $this->service = $service;
    }

    protected function getTickIntervalSeconds(): int
    {
        return 1;
    }

    protected function tick(): void
    {
        $this->service->exampleMethod();
    }
}
