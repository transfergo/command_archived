<?php declare(strict_types=1);

namespace TGF\Components\Command\Examples;

use Doctrine\ORM\EntityManager;
use TGF\Components\Command\AbstractQueueCommand;
use TGF\Components\Command\Examples\ExampleService;
use TGF\Util\IncomingQueue\Job;
use TGF\Util\IncomingQueue\Queue;
use TGF\Util\Logger\LoggerInterface;

class ExampleQueueCommand extends AbstractQueueCommand
{
    /** @var ExampleService */
    private $service;

    public function __construct(
        ExampleService $service,
        Queue $queue,
        string $commandName,
        LoggerInterface $logger,
        ?\PDO $pdo,
        ?EntityManager $entityManager
    ) {
        parent::__construct(
            $commandName,
            $queue,
            $logger,
            $pdo,
            $entityManager
        );

        $this->service = $service;
    }

    protected function handleJob(Job $job): void
    {
        $this->service->exampleMethod($job);
    }
}
