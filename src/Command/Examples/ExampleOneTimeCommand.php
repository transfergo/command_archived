<?php declare(strict_types=1);

namespace TGF\Components\Command\Examples;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TGF\Components\Command\AbstractCommand;
use TGF\Components\Command\Examples\ExampleService;
use TGF\Util\Logger\LoggerInterface;

class ExampleOneTimeCommand extends AbstractCommand
{
    /** @var ExampleService */
    private $service;

    public function __construct(
        ExampleService $service,
        string $commandName,
        LoggerInterface $logger,
        ?\PDO $pdo,
        ?EntityManager $entityManager
    ) {
        parent::__construct($commandName, $logger, $pdo, $entityManager);
        $this->service = $service;
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->service->exampleMethod();
    }
}
