FROM php:7.3.6-alpine

WORKDIR /var/www

RUN apk add curl bash zlib unzip git vim docker-bash-completion libzip-dev libxml2 libxml2-dev

RUN docker-php-ext-install pdo pdo_mysql zip soap bcmath pcntl

RUN curl -sS https://getcomposer.org/installer | php -- --filename=composer --install-dir=/usr/local/bin && \
    echo "export COMPOSER_HOME=/var/www/.composer" >> ~/.bashrc && \
    export COMPOSER_ALLOW_SUPERUSER=1 && \
    composer config -g github-oauth.github.com d8dbba78e0eb97cc101efc089e51ff225d1ac408

COPY ./composer.json ./composer.lock ./

RUN composer install --no-scripts --prefer-dist --no-autoloader

COPY ./ ./

RUN composer dump-autoload --optimize

