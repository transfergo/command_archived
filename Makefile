.PHONY: default
default: help


.PHONY: help
help: ## Get this help.
	@echo Tasks:
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

FORCE:

.PHONY: build
build:
	docker build -t tgf-workers:latest .

.PHONY: clean
clean: #s
	docker rmi -f tgf-workers

.PHONY: ssh
ssh: ## Connect to docker containers
	docker run -it tgf-workers bash

.PHONY: test
test: ## Run tests
	docker run -it tgf-workers ./run-tests.sh
