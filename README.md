TGF Command Utility classes
===================

# Installation

Add repository configuration to composer.json:

```json
{
    "repositories": {
        "transfergo/command": {
            "type": "vcs",
            "url": "https://bitbucket.org/transfergo/command.git"
         }
     },
     "require": {
        "transfergo/command": "^1.0"
     }
 }
```

# Contributing

* Project uses semantic versioning rules to tag changes
* Note what changes were made in CHANGELOG.md file

# Tagging a new version
1. Change "version" in property in composer.json file
2. `git tag -a {tag} -m 'message'`
    E.g.: `git tag -a 1.0.5 -m 'Some good update'`
3. `git push origin {tag}`